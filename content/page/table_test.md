---
title: table test
subtitle: 測試使用
comments: false
tableClasses: ["table", "table-striped", "table-dark"]
---

{{< bootstrap-table "table table-dark table-striped table-bordered" >}}
| Animal  | Sounds |
|---------|--------|
| Cat     | Meow   |
| Dog     | Woof   |
| Cricket | Chirp  |
{{< /bootstrap-table >}}

{{< table "table table-dark" >}}
|            PATREON PLEDGE            | 1€   | 5€   | 7€   |
|:------------------------------------ |:----:|:----:|:----:|
| Early Access to Articles             | ✓    | ✓    | ✓    |
| Hints for CryptoNovel Puzzles        | ✓    | ✓    | ✓    |
| Access to Patreon Feed               | ✓    | ✓    | ✓    |
| Small Listing On The Site As A Patron|      | ✓    | ✓    |
| Suggestions of New Content           |      | ✓    | ✓    |
| Request of a Research Article        |      |      | ✓    |
| Listed As A Full Sponsor             |      |      | ✓    |
{{< /table >}}

---
title: 測試
subtitle: 測試使用
comments: false
---

## Markdown表格與HTML語法使用

<table style="border:{1px solid blue};text-align:center;background-color:aqua">
    <thead>
        <tr>
            <th>教程标题</th>
            <th>主要内容</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>关于Markdown</td>
            <td>简介Markdown，Markdown的优缺点</td>
        </tr>
        <tr>
            <td>Markdown基础</td>
            <td>Markdown的<strong>基本语法</strong>，格式化文本、代码、列表、链接和图片、分割线、转义符等</td>
        </tr>
        <tr>
            <td>Markdown扩展</td>
            <td>Markdown的<strong>扩展语法</strong>，表格、公式、UML图</td>
        </tr>
    </tbody>
</table>

<table><tr><td bgcolor=red>背景颜色</td></tr></table>
<table><tr><td bgcolor=Gold>背景颜色</td></tr></table>
<table><tr><td bgcolor=LawnGreen>背景颜色</td></tr></table>

<font color=Black size=4 face="宋体">黑色Black</font>
<font color=Red size=4 face="宋体">红色Black</font>
<font color=Yellow size=4 face="宋体">黄色Black</font>
<font color=Blue size=4 face="宋体">蓝色Black</font>

## 樣式測試

<style>
  .purple {
    color:inherit;
  }
  .purple:hover {
    color:rgb(107,79,187);
  }
</style>

Hey! Hover the cursor over me and guess what?! :)
{: .purple}

***

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
The webcast I want to announce - [Register here][webcast-link]!
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

## table test

| Default aligned | Left aligned | Center aligned  | Right aligned  |
|-----------------|:-------------|:---------------:|---------------:|
| First body part | Second cell  | Third cell      | fourth cell    |
| Second line     | foo          | **strong**      | baz            |
| Third line      | quux         | baz             | bar            |
|-----------------+--------------+-----------------+----------------|
| Second body     |              |                 |                |
| 2nd line        |              |                 |                |
|-----------------+--------------+-----------------+----------------|
| Third body      |              |                 | Foo            |
{: .custom-class #custom-id}

{{< bootstrap-table "table table-dark table-striped table-bordered" >}}
|  段位  |  階級 | 星粹 |
|  ---  |  --- | --- |
|    |  戰神隊長  | 120 |
|    |  高級勝利隊長  | 96 |
|    |  中級勝利隊長  | 96 |
|    |  初級勝利隊長  | 96 |
|    |  高級堅毅隊長  | 76 |
|    |  中級堅毅隊長  | 76 |
|    |  初級堅毅隊長  | 76 |
|    |  高級勇氣隊長  | 60 |
|    |  中級勇氣隊長  | 60 |
|    |  初級勇氣隊長  | 60 |
{{< /bootstrap-table >}}
